# ImageSorter
## A simple tool for manipulating a image/video library
[![build status](https://gitlab.com/MaralAfris/ImageSorter/badges/master/build.svg)](https://gitlab.com/MaralAfris/ImageSorter/commits/master)
[![coverage report](https://gitlab.com/MaralAfris/ImageSorter/badges/master/coverage.svg)](https://gitlab.com/MaralAfris/ImageSorter/commits/master)

These tools have been tested on MacOS, it should be working for Linux systems
but hasn't been tested there. You are welcome to contribute :)

### ImageSorter
The goal of this tool is to do image analysis on a collection of picture to
determine which ones *might* be deleted.
Today tons of crappy pictures are taken (blurry, dark, broadly available) and
maybe we don't want to keep all of them. This tool helps to analyze the
pictures and determine which ones are *crappy*.

### Tools
A collection of scripts to help sort the picture/video library

#### Library Touch
Uses metadata from files as well as parent folder name to determine when
pictures and videos were taken and correct the OS creation date of the file
in order to keep the pictures in order.

#### Library transcode
Transcode (re-encode) videos from the library to a *newer* (and hopefully
*better*) compression format using FFmpeg.

## Installation (ImageSorter)
Install the required python packages (don't forget to use venv!):
```
pip3 install -r requirements.txt
```

## Installation (Tools)
First, install the required python packages (don't forget virtualenv):
```
pip3 install -r tools/requirements.txt
```

FFmpeg is needed in order to use library_transcode_cli.py.
I recommend building FFmpeg with all the options availale in order to limit
problems with the encoding.
Mac with Homebrew (https://trac.ffmpeg.org/wiki/CompilationGuide/macOS):
```
brew install ffmpeg --with-x265 --with-libx265 --with-libvpx \
                    --with-fdk-aac --with-freetype --with-libass --with-tools \
                    --with-fdk-aac --with-freetype --with-fontconfig \
                    --with-libass --with-libvorbis --with-opus
```

## Usage (ImageSorter)
```
./image_sorter_cli.py pictures_folder
```

Get help
```
./image_sorter_cli.py -h
```

Recursively list blurry images with threshold 50.5
```
./image_sorter_cli.py pictures_folder -r -bt 50.5
```

Delete files that are blurry:
```
./image_sorter_cli.py pictures_folder -r -ns -rm
```

## Usage (Tools)

### Library touch

Simulate correcting date of images using metadata and/or parent folder name:
```
./library_touch_cli.py pictures_folder -s
```

Simulate correcting date of images using metadata and/or parent folder name and export choice:
```
./library_touch_cli.py pictures_folder -s -o choices.csv
or
./library_touch_cli.py pictures_folder -s -o choices.json
```

Simulate correcting date of images using metadata and/or parent folder name and apply changes:
```
./library_touch_cli.py pictures_folder -s -o choices.csv -e
```

### Library transcode
Parse the folders and choose what to do (default is transcode to hevc/h.265):
```
./library_touch_cli.py video_folder log_output_file.csv
```

Choose what to do (default is transcode to hevc/h.265) and execute:
```
./library_touch_cli.py video_folder log_output_file.csv -e
```

Choose what to do (default is transcode to hevc/h.265), execute and remove
original videos that have been transcoded:
```
./library_touch_cli.py video_folder log_output_file.csv -e -r
```

Choose what to do (default is transcode to hevc/h.265), execute, remove
original videos that have been transcoded and discard codec versions:
```
./library_touch_cli.py video_folder log_output_file.csv -e -r -f
```

Add a prefix and suffix to the output filename:
```
./library_transcode_cli.py video_folder log_output_file.csv -p "vid_" -s "_h265" -x acodec=copy,preset=fast
```

Provide specific FFmpeg parameters for transcoding (overwrites default): \
<b>Be careful with this, unrecognized parameters will trigger transcode failure.</b>
```
./library_transcode_cli.py video_folder log_output_file.csv -x acodec=copy,preset=fast,vcodec=mpeg1
```

### ffmpeg_batch_encode

Batch encode files in a directory using ffmpeg.

Call the computer's built-in ffmpeg to encode files in a folder.

Encode every files with the extension(s) provided
Output them to the ouput folder
Use options provided during encode

Can use custom path to ffmpeg build

#### Usage

    $ ./ffmpeg-batch.py -i input/folder -o output/folder -e mp4 -f /ffmpeg/path
    $ ./ffmpeg-batch.py -i input/folder -o output/folder -e mp4 -p vcodec libx264

#### Example of options supported

vcodec libx264
tune film
ab 192k
threads 0
b_strategy 2
flags2 +bpyramid
refs 32
flags +loop
crf 26
maxrate 500000
flags2 +wpred
me_method umh
me_range 32
subq 16
flags2 +mixed_refs
flags2 +dct8x8
trellis 2

#### Important notice
Does not support options with dash (ex: flags2 -fastpskip)