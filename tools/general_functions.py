#!/usr/bin/env python
# coding: utf-8
"""
Functions used by other parts of the project
"""
import os

#------------------------------------------------------------------------------
# list_files_in_dir
#------------------------------------------------------------------------------
def list_files_in_dir(root_path, filetypes):
    """
    List files in directory and subdirectory of path and return them in a list
    root_path is a string of the path to walk into
    filetypes is a list of the files filetypes (without dot)
    """
    #Do some check on arguments
    if (root_path is None or
        filetypes is None or
        not os.path.isdir(root_path) or
        not isinstance(filetypes, (list, tuple))
        ):
        print "ERROR: list_files_in_dir: Arguments provided don't seem coherent"
        return -1

    #format strings
    filetypes = [i if not i.startswith('.') else i[1:] for i in filetypes]

    total_list = []
    for path, subdirs, files in os.walk(root_path, topdown=True):
        for name in files:
            if os.path.splitext(name)[1][1:] in filetypes:
                total_list.append(os.path.join(path, name))
    return total_list

if __name__ == "__main__":
    print("This part is not meant to be called directly")

#------------------------------------------------------------------------------
# true_or_flase
#------------------------------------------------------------------------------
def true_or_flase(arg):
    """
    Converts string into boolean
    """
    #Do some check on arguments
    if arg is not str:
	print("ERROR: true_or_flase : unexpected data type")
	return -1
    value = str(arg).upper()
    if value == 'FALSE'[:len(value)]:
        return False
    else:
        return True
