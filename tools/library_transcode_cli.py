#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Alexandre Martins (a.k.a Kodsama)
#
# This file is part of ImageSorter.
#
# ImageSorter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ImageSorter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ImageSorter.  If not, see <http://www.gnu.org/licenses/>.
"""
Re-encode videos in a folder using ffmpeg
Usable from command line.
"""

import os
import ntpath
import argparse
import logging
import coloredlogs
import json
import csv
import ffmpeg

from termcolor import cprint


# Create a logger object.
logger = logging.getLogger(__name__)
coloredlogs.install(level='DEBUG', logger=logger)
logger.setLevel(logging.INFO)


SUPPORTED_EXT = ['.avi', '.mp4', '.mpeg', '.mpg',
                 '.mts', '.mov', '.3gp', '.asf']
# Codecs are listed by order of importance (newest to oldest)
SUPPORTED_IN_CODECS = ['hevc', 'h264', 'mpeg4',
                       'mjpeg4', 'mpeg1video', 'mjpeg',
                       'svq3', 'wmv3']
SUPPORTED_OUT_CODECS = ['hevc', 'h264']


class file_attribute(object):
    """
    File information class. Contains file path, type and new path
    """
    def __init__(self, p: str, new_path: str = None, codec: str = ''):
        absp = os.path.abspath(p)
        assert os.path.exists(absp)
        name, ext = os.path.splitext(absp)
        parent, name = ntpath.split(name)

        typ = 'dir' if os.path.isdir(absp) else 'file'

        self.path = absp                # Complete path
        self.type = typ                 # Type
        self.parent = parent            # Parent path
        self.name = name                # Filename
        self.extension = ext.lower()    # Extension
        self.new_path = new_path        # Output path
        self.code = codec               # Codec name
        self.proceed = False            # Proceed with transcoding?
        self.proceed_reason = ''        # Why not proceeding?
        self.succeeded = False          # Success of transcoding?
        self.succeeded_reason = ''      # Why not succeeding?

    def __str__(self) -> str:
        s = '{} ({}): {} -> {}'.format(self.path, self.type,
                                       self.codec, self.new_path)
        return s

    def generate_new_path(self, prefix: str = '', suffix: str = '',
                          new_path: str = None, new_ext: str = None):
        new = '{}{}{}'.format(prefix, self.name, suffix)
        new += new_ext if new_ext else self.extension
        if new_path and os.path.isdir(new_path):
            self.new_path = os.path.join(new_path, new)
        else:
            self.new_path = os.path.join(self.parent, new)
        return self.new_path


def query_yes_no(question: str, default: str = 'no'):
    """
    Ask a yes/no question via raw_input() and return their answer.

    'question' is a string that is presented to the user.
    'default' is the presumed answer if the user just hits <Enter>.
        It must be 'yes' (the default), 'no' or None (meaning
        an answer is required of the user).

    The 'answer' return value is True for 'yes' or False for 'no'.
    """
    valid = {'yes': True, 'y': True, 'ye': True,
             'no': False, 'n': False}
    if default is None:
        prompt = ' [y/n] '
    elif default == 'yes':
        prompt = ' [Y/n] '
    elif default == 'no':
        prompt = ' [y/N] '
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        cprint(question + prompt, 'blue')
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            cprint("Please respond with 'yes' or 'no' (or 'y' or 'n').\n",
                   'red')


# Print iterations progress, see:
# https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
def print_progressbar(iteration: int, total: int,
                      prefix: str = '', suffix: str = '', decimals: int = 1,
                      length: int = 80, fill: str = '█', color: str = 'white'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        color        - Optional  : bar color (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(
               100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    cprint('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix),
           color, end='\r')
    # Print New Line on Complete
    if iteration == total:
        cprint('')


def get_objects_from_path(path: str, extensions: list = None) -> list:
    """
    Get a list of files in the specified path
    """
    assert os.path.exists(path)
    assert extensions is None or isinstance(extensions, list)
    assert not os.path.isfile(path)
    path = os.path.abspath(path)

    # Get directories to determine os.walk progress
    total = len(list(os.walk(path)))

    # Get file list
    objects = []
    cnt = 0
    for directory, _, file_list in os.walk(path):
        # Print progress
        cnt += 1
        print_progressbar(cnt, total, prefix='1. List files')

        # Walk through
        directory = os.path.abspath(directory)
        objects.append(file_attribute(directory, 'dir'))
        for f in file_list:
            f = os.path.abspath(os.path.join(path, directory, f))
            objects.append(file_attribute(f, 'file'))
    return objects


def save_changes_list_to_file(data: list, output: str,
                              interract: bool = True) -> bool:
    """
    Save the data and decision to a file (.json or .csv)
    """
    assert isinstance(data, list)
    assert len(data) > 0
    assert isinstance(output, str)

    out_file = os.path.abspath(output)
    if os.path.exists(out_file):
        if interract:
            if not query_yes_no('File {} exists, overwrite?'.format(out_file)):
                return False
        else:
            raise(IOError('File {} already exists'.format(out_file)))

    # Manually serialize
    json_cpy = []
    for d in data:
        assert isinstance(d, file_attribute)
        json_cpy.append(
            {'source path': d.path,
             'source type': d.type,
             'process ?': d.proceed,
             'succeeded ?': d.succeeded,
             'target path': d.new_path,
             'source codec name': d.codec,
             'reason for not processing': d.proceed_reason,
             'transcoding failure reason': d.succeeded_reason})

    if output.endswith('.json'):
        with open(out_file, 'w', newline='') as fout:
            json.dump(json_cpy, fout)
    elif output.endswith('.csv'):
        keys = json_cpy[0].keys()
        with open(out_file, 'w', newline='') as fout:
            dict_writer = csv.DictWriter(fout, keys)
            dict_writer.writeheader()
            dict_writer.writerows(json_cpy)
    else:
        logger.error('Not recognized file extension for {}'.format(output))
        return False

    logger.info('Created data look fine, wrote to file {}'.format(out_file))
    return True


def get_video_codec(video: str) -> str:
    """
    Extract information from a video using ffprobe.
    """
    name = ''
    try:
        stream = ffmpeg.probe(video)
    except Exception as e:
        logger.debug('Processing {} raised: {}'.format(video, e))
    else:
        # Extract the codec_name from the first video stream
        for s in stream['streams']:
            if s['codec_type'] == 'video':
                name = s['codec_name']
                break
    return name


def do_ffmpeg_transcode(obj: file_attribute, v_codec: str = 'hevc',
                        ffmpeg_extra: dict = {}) -> (bool, str):
    """
    Apply changes to files according to plan. Plan is a list of file_attribute
    """
    # Check data provided first (you never know)
    assert isinstance(obj, file_attribute)
    assert v_codec in SUPPORTED_OUT_CODECS
    assert isinstance(ffmpeg_extra, dict)

    # Output parameters (check forum.doom9.org/showthread.php?t=172458)
    ffmpeg_param = {
        'vcodec': v_codec,
        'preset': 'slower',
        'crf': '28',
        'acodec': 'aac',
        'ab': '160k',
        'map_metadata': '0',
        'map_chapters': '0',
        'scodec': 'copy',
        'loglevel': 'panic',
        }
    ffmpeg_param.update(ffmpeg_extra)
    logger.debug('Run ffmpeg: {} -> {} with parameters: {}'.format(
                 obj.path, obj.new_path, ffmpeg_param))

    try:
        (ffmpeg
            .input(obj.path)
            .output(obj.new_path, **ffmpeg_param)
            .global_args('-n')
            # .overwrite_output()
            .run()
        )
    except Exception as e:
        logger.warning('Error while transcoding {}: {}'.format(obj.path, e))
        try:
            os.remove(obj.new_path)
        except FileNotFoundError:
            pass
        return False, e
    return True, 'Transcoding suceeded'


def transcode_recur(root_path: str, log: str, execute: bool = False,
                    prefix: str = '', suffix: str = '',
                    new_path: str = None, codec_name: str = 'hevc',
                    remove: bool = False, force: bool = False,
                    ffmpeg_extra: dict = {}):
    """
    Recursively transcode files in folder and subfolder
    """
    # Do extensive checks (because it is called from cli normally)
    assert isinstance(root_path, str)
    assert log is None or isinstance(log, str)
    assert new_path is None or isinstance(new_path, str)
    assert os.path.isdir(root_path)
    assert isinstance(prefix, str) and isinstance(prefix, str)
    assert isinstance(codec_name, str) and codec_name in SUPPORTED_OUT_CODECS
    assert isinstance(ffmpeg_extra, dict)

    path_obj = get_objects_from_path(root_path)
    cnt_dir = len([o for o in path_obj if o.type == 'dir'])
    total = len(path_obj)
    logger.info('Found {} objects: {} dirs and {} files'.format(
                total, cnt_dir, total - cnt_dir))

    # Extract only supported files
    vid_obj = [o for o in path_obj if o.extension in SUPPORTED_EXT]
    total = len(vid_obj)
    logger.info('Found {} videos'.format(total))

    cnt = 0
    # Get video information
    for o in vid_obj:
        o.codec = get_video_codec(o.path)
        cnt += 1
        print_progressbar(cnt, total, prefix='2. Get video info')

    # Determine changes
    cnt = 0
    for o in vid_obj:
        o.generate_new_path(prefix=prefix, suffix=suffix,
                            new_path=new_path, new_ext='.mp4')
        # Shall we proceed ?
        o.proceed = True
        if os.path.exists(o.new_path):
            o.proceed = False
            o.proceed_reason = 'File {} already exists'.format(o.new_path)

        if o.codec not in SUPPORTED_IN_CODECS:
            o.proceed = False
            o.proceed_reason = 'Codec \'{}\' not supported'.format(o.codec)

        if o.proceed and not force:
            in_idx = SUPPORTED_IN_CODECS.index(o.codec)
            out_idx = SUPPORTED_OUT_CODECS.index(codec_name)
            # If the original codec is at least as new as out codec, skip
            if in_idx <= out_idx:
                o.proceed = False
                o.proceed_reason = 'Codec \'{}\' not newer than {}'.format(
                                   o.codec, codec_name)

        cnt += 1
        print_progressbar(cnt, total, prefix='3. Determine changes')

    plan_to_process = len([o for o in vid_obj if o.proceed])
    logger.info('Decided to transcode {} videos ({} skipped)'.format(
                plan_to_process, total - plan_to_process))

    # Save to file for review
    file_export = save_changes_list_to_file(vid_obj, log, interract=True)

    # Do use check before proceeding
    if not execute:
        return

    if not file_export:
        logger.error('Change list failed to save. Can\'t review. Abort')
        return
    elif not query_yes_no('About to transcode, do you want to proceed?'):
        return

    # Transcode
    to_process = [o for o in vid_obj if o.proceed]
    cnt = 0
    total = len(to_process)

    ret = False
    # Go through all videos in order to be able to output a final log
    for o in vid_obj:
        if o.proceed:
            o.succeeded, o.succeeded_reason = do_ffmpeg_transcode(
                                                o, codec_name,
                                                ffmpeg_extra=ffmpeg_extra)
            if o.succeeded and remove:
                os.remove(o.path)
            cnt += 1
            print_progressbar(cnt, total, prefix='4. Transcode')

    # Output final log
    if query_yes_no('Would you like to save the final log?'):
        save_changes_list_to_file(vid_obj, log, interract=True)

    return


if __name__ == '__main__':
    desc = 'Video library transcode utility'

    # Allow to save dictionnary
    my_dict = {}
    class StoreDictKeyPair(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            for kv in values.split(','):
                k, v = kv.split('=')
                my_dict[k] = v
            setattr(namespace, self.dest, my_dict)

    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(type=str,
                        default='', dest='root_path',
                        help='Path to process')
    parser.add_argument(type=str,
                        default='', dest='log',
                        help='Write decision to a file for review')
    parser.add_argument('-e', '--execute', action='store_true',
                        default=False, dest='execute',
                        help='Execute the changes instead of simulating')
    parser.add_argument('-p', '--prefix',
                        default='', dest='prefix',
                        help='Prefix to appen to new files')
    parser.add_argument('-s', '--suffix',
                        default='', dest='suffix',
                        help='Suffix to appen to new files')
    parser.add_argument('-c', '--codec',
                        default='hevc', dest='codec',
                        help='Video codec to use')
    parser.add_argument('-n', '--new_path',
                        default=None, dest='new_path',
                        help='Output all files to a specific folder')
    parser.add_argument('-r', '--remove_originals', action='store_true',
                        default=False, dest='remove_originals',
                        help='Delete originals after transcoding?')
    parser.add_argument('-f', '--force_trancode', action='store_true',
                        default=False, dest='force_trancode',
                        help='Delete originals after transcoding?')
    parser.add_argument('-x', '--ffmpeg_extra', action=StoreDictKeyPair,
                        default={}, dest='ffmpeg_extra_dict',
                        metavar='KEY1=VAL1,KEY2=VAL2...',
                        help='FFmpeg parameters (will replace default)')
    args = parser.parse_args()

    # Do stuff
    transcode_recur(args.root_path, args.log, execute=args.execute,
                    prefix=args.prefix, suffix=args.suffix,
                    new_path=args.new_path, codec_name=args.codec,
                    remove=args.remove_originals, force=args.force_trancode,
                    ffmpeg_extra=args.ffmpeg_extra_dict)
    logger.info('Processing done !')
    exit()
