#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Alexandre Martins (a.k.a Kodsama)
#
# This file is part of ImageSorter.
#
# ImageSorter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ImageSorter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ImageSorter.  If not, see <http://www.gnu.org/licenses/>.
"""
Handle file related actions
"""

import os


def get_file_paths(path: str = None, ext: list = None, recur: bool = False):
    """
    List all files in p with extension t and it's subfolders.
    Returns a list of the files found.
    Args:
        path (str): string of the path to get the images from
        ext (list): list of extensions to consider
        recur (bool): Recursive (True/False)
    Returns:
        (list(str)) list of file paths to handle
    """
    out = []
    # Check input
    if (not isinstance(path, str) or 
        not isinstance(ext, list) or
        not isinstance(recur, bool)):
        print('Please provide all parameters: {}, {}, {}'.format(path, ext, recur))
        return out
    if not os.path.isdir(path):
        print('Please provide a directory ({})'.format(path))
        return out

    # Go through dir (and subdir)
    text = tuple(ext)
    for root, dirs, files in os.walk(path):
        if not recur:  
            while len(dirs):  
                dirs.pop()
        for file in files:
            if file.endswith(text):
                out.append(os.path.join(root, file))
    return out


def remove_files(fl: list = None):
    """
    Remove files provided in fl
    Args:
        fl (list(str)): list of files to remove
    Returns:
        Nothing
    """
    if not isinstance(fl, list):
        print('Please provide a list of files not a {}'.format(type(fl)))
        return
    
    for f in fl:
        os.remove(os.path.abspath(f))
