#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Alexandre Martins (a.k.a Kodsama)
#
# This file is part of ImageSorter.
#
# ImageSorter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ImageSorter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ImageSorter.  If not, see <http://www.gnu.org/licenses/>.
"""
Image detection functions (blur, noise...)
"""

import numpy as np
import cv2
import imagehash
from PIL import Image


def load_image(img: str) -> tuple:
    """
    Load and return the loaded image using opencv imread
    Args:
        img (str): image path
    Returns:
        (np.ndarray, np.ndarray, PIL.Image.Image, Tuple(int, int)) Loaded image data
    """
    image = cv2.imread(img)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    pil = Image.fromarray(image)
    return (image, gray, pil, image.shape)


def bad_histogram(img: np.ndarray = None, threshold: float = 10.0, distance: int = 100, edges_dist: int = 100) -> bool:
    """
    Computes the histogram and returns if the histogram looks ok
    Args:
        img (np.ndarray): loaded colored image data
        threshold (float): Threshold to consider the histogram values to be considered or not
        distance (int): maximum distance between two values in the histogram to be ok
        edges_dist (int): maximum distance from the beginning or end the histogram to be ok
    Returns:
        (bool): Filter choice
    """
    hist = cv2.calcHist(img,[0],None,[256],[0,256])
    thr_indices = hist < threshold
    hist[thr_indices] = 0.0
    # Get max distance between values, brightness and darkness
    void_max = 0
    void_dark = None
    void_bright = 0
    void_cnt = 0
    for v in hist:
        if not v:
            void_cnt +=1
        else:
            # Get max distance
            if void_cnt > void_max:
                void_max = void_cnt
            # First count gives us the bright area (end of histogram)
            if void_dark is None:
                void_dark = void_cnt
            void_cnt = 0
    # Last count gives us the bright area (end of histogram)
    void_bright = void_cnt

    # Simple logic choice
    bad = void_max >= distance or void_dark >= edges_dist or void_bright >= edges_dist
    return bad


def is_blurry(gray: np.ndarray = None, threshold: float = 100.0) -> bool:
    """
    Computes and returns the blur level of each images
    Args:
        gray (np.ndarray): loaded grayscaled image data
        threshold (float): Threshold to consider the image as blurry or not
    Returns:
        (bool): Filter choice
    """
    def variance_of_laplacian(image: np.ndarray):
        """
        Compute the Laplacian of the image and then return the focus
        measure, which is simply the variance of the Laplacian.
        Args:
            image (np.ndarray): Grayscaled image from imread
        Returns:
            (tuple) Variance of the Laplacian
        """
        return cv2.Laplacian(image, cv2.CV_64F).var()

    fm = variance_of_laplacian(gray)
    # if the focus measure is less than the supplied threshold,
    # then the image should be considered "blurry"
    blurry = True if fm < threshold else False
    return blurry


def has_similar_hash(hash1: str, hash2: str, similarity: float = 0.9, hash_size: int = 16) -> bool:
    threshold = 1 - similarity
    diff_limit = int(threshold * (hash_size ** 2))
    if np.count_nonzero(hash1 != hash2) <= diff_limit:
        return True
    return False


def get_hash(img_pil: np.ndarray = None, hash_size: int = 16) -> list:
    return imagehash.average_hash(img_pil, hash_size).hash