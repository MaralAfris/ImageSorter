#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Alexandre Martins (a.k.a Kodsama)
#
# This file is part of ImageSorter.
#
# ImageSorter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ImageSorter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ImageSorter.  If not, see <http://www.gnu.org/licenses/>.
"""
Command line tool
"""

import logging
from os.path import abspath, basename
from termcolor import colored
import pandas as pd

import image_sorter.file_handler as fh
import image_sorter.image_handler as imd


class ImageSorterOptions(object):
    """ Contains ImageSorter options """
    def __init__(self, **kw):
        self.path = abspath(kw.get('path', None))
        self.simulate = kw.get('simulate', True)
        self.recursive = kw.get('recursive', False)
        self.delete = kw.get('delete', False)
        self.types = kw.get('types', ['.jpg', '.png', '.jpeg'])
        # What to run
        self.check_blur = not kw.get('no_blur', False)
        self.check_hist = not kw.get('no_hist', False)
        self.check_hash = not kw.get('no_hash', False)
        # Simulate overwrites delete
        self.delete = False if self.simulate else self.delete


class ImageSorter(object):
    """ ImageSorter object """
    def __init__(self, **kw):
        self.logger = logging.getLogger('')
        self.o = ImageSorterOptions(**kw)
        self.files = pd.DataFrame({}, columns=['path', 'filename', 'size', 'hash', 'duplicates', 'blurry', 'bad_histogram', 'delete'])

    def run(self):
        """ Load images, loops through them and  """
        self.files['path'] = fh.get_file_paths(self.o.path, self.o.types, recur=self.o.recursive)
        self.files['filename'] = [basename(x) for x in self.files.path]
        # Gather information about the image
        for _, row in self.files.iterrows():
            i, g, p, row['size'] = imd.load_image(row['path'])
            # Do filtering
            if self.o.check_blur:
                row['blurry'] = imd.is_blurry(g)
            if self.o.check_hist:
                row['bad_histogram'] = imd.bad_histogram(i)
            if self.o.check_hash:
                row['hash'] = imd.get_hash(p)
    
        # We have all hashes, check for duplicates
        if self.o.check_hash:
            for _, row1 in self.files.iterrows():
                for _, row2 in self.files.iterrows():
                    if row1.path == row2.path:
                        continue    # Do not check itself
                    if imd.has_similar_hash(row1.hash, row2.hash):
                        row1['duplicates'] = row1.duplicates.append(row2.path) if isinstance(row1.duplicates, list) else [row2.path]
                        break

    def reset(self):
        """ Reset lists """
        self.files = []

    def print(self):
        """ Print the results """
        print(colored('Results:', 'cyan'))
        print(self.files)
        print(colored('To be deleted:', 'red'))
        self.__build_delete()
        print([r.filename for _, r in self.files.iterrows() if r.delete])

    def __build_delete(self):
        """ Generate the to_delete list from results """
        self.files['delete'] = self.files.duplicates.apply(lambda x: isinstance(x, list))

    def delete(self):
        """ Delete files on the to_delete_list """
        self.__build_delete()
        if self.o.delete:
            fh.remove_files(self.to_delete)